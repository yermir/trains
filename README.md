# Title

Train route checking 

## Getting Started

This module lets you calculate the route among several configured stops.

   
### Prerequisites

* Java 8
* Gradle 4.6

## Running the tests

Go to the test folder and run the tests using any id. In my case I used intellj

## Authors

* **Yermir Rivero** 