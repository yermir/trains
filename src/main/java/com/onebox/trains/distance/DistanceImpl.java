package com.onebox.trains.distance;

import com.onebox.trains.model.Stop;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DistanceImpl implements Distance {

    private static final String SEPARATOR = "-";
    private static final int UNKNOWN_DISTANCE = 0;
    private static final int UNKNOWN_STOP = -1;
    private static final String NO_SUCH_ROUTE = "NO SUCH ROUTE";

    @Override
    public int calculateDistance(String input) {

        MockConfiguredRoutes mockInstance = MockConfiguredRoutes.getInstance();


        String[] inputRoutes = input.split(SEPARATOR);
        List<Stop> stopsToBeProcessed = new ArrayList<>();
        Stop stop;
        int originIndex = 0;

        for (int destinyIndex = 1; destinyIndex < inputRoutes.length; destinyIndex++) {
            stop = createStop(inputRoutes[originIndex], inputRoutes[destinyIndex]);

            try {
                if (!isAConfiguratedStop(mockInstance.getConfiguredRoutes(), stop)) {
                    clearStops(stopsToBeProcessed);
                    throw new DistanceException(NO_SUCH_ROUTE);
                }
                addExistingStop(mockInstance.getConfiguredRoutes(), stopsToBeProcessed, stop);
                originIndex++;
            } catch (DistanceException e) {
                if (e.getMessage().equals(NO_SUCH_ROUTE)) {
                    e.printStackTrace();
                    return UNKNOWN_DISTANCE;
                }
            }
        }
        return getTotalLength(stopsToBeProcessed);
    }

    private int getTotalLength(List<Stop> stopInput) {
        return stopInput.stream()
                .collect(Collectors.summingInt(Stop::getDistance));
    }

    private void addExistingStop(List<Stop> graphs, List<Stop> stopsToBeProcessed, Stop stop) {
        stopsToBeProcessed.add(graphs.get(graphs.indexOf(stop)));
    }

    private void clearStops(List<Stop> stopsToBeProcessed) {
        stopsToBeProcessed.clear();
    }

    private boolean isAConfiguratedStop(List<Stop> graphs, Stop stop) {
        return UNKNOWN_STOP != graphs.indexOf(stop);
    }

    private Stop createStop(String origin, String destiny) {
        return new Stop(origin + destiny, UNKNOWN_DISTANCE);
    }
}
