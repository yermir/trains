package com.onebox.trains.distance;

import com.onebox.trains.model.Stop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MockConfiguredRoutes {

    private List<Stop> configuredRoutes;

    private MockConfiguredRoutes() {
        Stop stopAB = new Stop("AB", 5);
        Stop stopBC = new Stop("BC", 4);
        Stop stopCD = new Stop("CD", 8);
        Stop stopDC = new Stop("DC", 8);
        Stop stopDE = new Stop("DE", 6);
        Stop stopAD = new Stop("AD", 5);
        Stop stopCE = new Stop("CE", 2);
        Stop stopEB = new Stop("EB", 3);
        Stop stopAE = new Stop("AE", 7);

        configuredRoutes = new ArrayList<>(Arrays.asList(stopAB, stopBC, stopCD, stopDC, stopDE, stopAD, stopCE, stopEB, stopAE));

    }

    private static class SingletonMockConfiguredRoutesHolder {
        private static final MockConfiguredRoutes INSTANCE = new MockConfiguredRoutes();
    }

    static MockConfiguredRoutes getInstance() {
        return SingletonMockConfiguredRoutesHolder.INSTANCE;
    }

    public List<Stop> getConfiguredRoutes() {
        return configuredRoutes;
    }
}
