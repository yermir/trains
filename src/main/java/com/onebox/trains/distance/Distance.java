package com.onebox.trains.distance;

public interface Distance {

    int calculateDistance(String input);
}
