package com.onebox.trains.distance;

public class DistanceException extends Exception {

    public DistanceException(String message) {
        super(message);
    }

}
