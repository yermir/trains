package com.onebox.trains.model;

import java.util.Objects;

public class Stop {
    private String route;
    private Integer distance;

    public Stop(String route, Integer distance) {
        this.route = route;
        this.distance = distance;
    }

    public Integer getDistance() {
        return distance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stop stop1 = (Stop) o;
        return Objects.equals(route, stop1.route);
    }

    @Override
    public int hashCode() {
        return Objects.hash(route);
    }
}
