package com.onebox.trains;

import com.onebox.trains.distance.Distance;
import com.onebox.trains.distance.DistanceImpl;
import org.junit.Assert;
import org.junit.Test;

public class TrainTest {

    private Distance sut = new DistanceImpl();

    @Test
    public void routeABC() {
        Assert.assertEquals(9, sut.calculateDistance("A-B-C"));
    }

    @Test
    public void routeAD() {
        Assert.assertEquals(5, sut.calculateDistance("A-D"));
    }

    @Test
    public void routeADC() {
        Assert.assertEquals(13, sut.calculateDistance("A-D-C"));
    }

    @Test
    public void routeAEBCD() {
        Assert.assertEquals(22, sut.calculateDistance("A-E-B-C-D"));
    }

    @Test
    public void notExistingRouteAED() {
        Assert.assertEquals(0, sut.calculateDistance("A-E-D"));
    }

    @Test
    public void notExistingRouteACB() {
        Assert.assertEquals(0, sut.calculateDistance("A-C-B"));
    }

    @Test
    public void notExistingRouteAA() {
        Assert.assertEquals(0, sut.calculateDistance("A-A"));
    }
}
